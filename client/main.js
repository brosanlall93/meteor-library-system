import { Template } from 'meteor/templating';
import { Books } from '../imports/books.js';
import './main.html';

Template.body.helpers({
	books(){
		return Books.find({});
	},
});

Template.body.events({
	'submit form'(event){
		//Prevent the default browser form submit
		event.preventDefault();

		//get the value from the form
		const target = event.target;
		const Dtitle = target.title.value;
		const Dauthor = target.author.value;
		const DpublishedDate = target.publishDate.value;

		Books.insert({
			title: Dtitle,
			author: Dauthor,
			publishDate: DpublishedDate,
		});

		//Clear out the boxes
		target.title.value = "";
		target.author.value = "";
		target.publishDate.value = "";
	},
});